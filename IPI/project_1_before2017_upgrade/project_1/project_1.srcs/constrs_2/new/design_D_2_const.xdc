#Clock constraints
create_clock -period 10.000 -name sys_clock [get_ports sys_clock]

create_clock -period 40.000 -name eth_phy_clk [get_ports eth_phy_clk]

create_clock -period 40.000 -name eth_mii_tx_clk [get_nets design_1_i/eth_mii_tx_clk]
create_clock -period 40.000 -name eth_mii_rx_clk [get_nets design_1_i/eth_mii_rx_clk]


set_property IOSTANDARD DIFF_SSTL135 [get_ports {ddr3_sdram_ck_p[0]}]



#set_property PACKAGE_PIN E3 [get_ports sys_clock]
set_property IOSTANDARD LVCMOS33 [get_ports sys_clock]

##Ethernet clock
set_property PACKAGE_PIN G18 [get_ports eth_phy_clk]
set_property IOSTANDARD LVCMOS33 [get_ports eth_phy_clk]

#Ethernet RJ45 (J2)
set_property IOB TRUE [get_ports eth_mii_col]
set_property IOB TRUE [get_ports eth_mii_crs]
set_property IOB TRUE [get_ports eth_mii_rx_dv]
set_property IOB TRUE [get_ports eth_mii_rx_er]
set_property IOB TRUE [get_ports {eth_mii_rxd[3]}]
set_property IOB TRUE [get_ports {eth_mii_rxd[2]}]
set_property IOB TRUE [get_ports {eth_mii_rxd[1]}]
set_property IOB TRUE [get_ports {eth_mii_rxd[0]}]
set_property IOB TRUE [get_ports eth_mii_tx_en]
set_property IOB TRUE [get_ports {eth_mii_txd[3]}]
set_property IOB TRUE [get_ports {eth_mii_txd[2]}]
set_property IOB TRUE [get_ports {eth_mii_txd[1]}]
set_property IOB TRUE [get_ports {eth_mii_txd[0]}]



set_property PACKAGE_PIN D3 [get_ports spi_0_io0_io]
set_property IOSTANDARD LVCMOS33 [get_ports spi_0_io0_io]
set_property PACKAGE_PIN D2 [get_ports spi_0_io1_io]
set_property IOSTANDARD LVCMOS33 [get_ports spi_0_io1_io]
set_property PACKAGE_PIN D4 [get_ports spi_0_io2_io]
set_property IOSTANDARD LVCMOS33 [get_ports spi_0_io2_io]
set_property PACKAGE_PIN H2 [get_ports spi_0_io3_io]
set_property PACKAGE_PIN F3 [get_ports spi_0_sck_io]
set_property IOSTANDARD LVCMOS33 [get_ports spi_0_io3_io]
set_property IOSTANDARD LVCMOS33 [get_ports spi_0_sck_io]
set_property PACKAGE_PIN G2 [get_ports {spi_0_ss_io[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {spi_0_ss_io[0]}]
set_property PACKAGE_PIN A11 [get_ports {gpio2_2_tri_io[0]}]
set_property PACKAGE_PIN B18 [get_ports {gpio2_2_tri_io[1]}]
set_property PACKAGE_PIN D13 [get_ports {gpio2_2_tri_io[2]}]

set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_2_tri_io[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_2_tri_io[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_2_tri_io[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {dip_switches_4bits_tri_i[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {dip_switches_4bits_tri_i[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {dip_switches_4bits_tri_i[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {dip_switches_4bits_tri_i[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_1_tri_io[13]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_1_tri_io[12]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_1_tri_io[11]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_1_tri_io[10]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_1_tri_io[9]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_1_tri_io[8]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_1_tri_io[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_1_tri_io[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_1_tri_io[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_1_tri_io[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_1_tri_io[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_1_tri_io[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_1_tri_io[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_1_tri_io[0]}]
set_property PACKAGE_PIN R12 [get_ports {gpio2_1_tri_io[1]}]
set_property PACKAGE_PIN T14 [get_ports {gpio2_1_tri_io[2]}]
set_property PACKAGE_PIN T15 [get_ports {gpio2_1_tri_io[3]}]
set_property PACKAGE_PIN T16 [get_ports {gpio2_1_tri_io[4]}]
set_property PACKAGE_PIN N15 [get_ports {gpio2_1_tri_io[5]}]
set_property PACKAGE_PIN R15 [get_ports {gpio2_1_tri_io[6]}]
set_property PACKAGE_PIN R13 [get_ports {gpio2_1_tri_io[7]}]
set_property PACKAGE_PIN R11 [get_ports {gpio2_1_tri_io[8]}]
set_property PACKAGE_PIN R10 [get_ports {gpio2_1_tri_io[9]}]
set_property PACKAGE_PIN M13 [get_ports {gpio2_1_tri_io[10]}]
set_property PACKAGE_PIN V16 [get_ports {gpio2_1_tri_io[11]}]
set_property PACKAGE_PIN U11 [get_ports {gpio2_1_tri_io[12]}]
set_property PACKAGE_PIN V15 [get_ports {gpio2_1_tri_io[13]}]

set_property IOSTANDARD LVCMOS33 [get_ports {spi_cfg_ss_io[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports spi_cfg_io0_io]
set_property IOSTANDARD LVCMOS33 [get_ports spi_cfg_io1_io]
set_property IOSTANDARD LVCMOS33 [get_ports spi_cfg_io2_io]
set_property IOSTANDARD LVCMOS33 [get_ports spi_cfg_io3_io]
set_property IOSTANDARD LVCMOS33 [get_ports spi_cfg_sck_io]
set_property PACKAGE_PIN L13 [get_ports {spi_cfg_ss_io[0]}]
set_property PACKAGE_PIN K17 [get_ports spi_cfg_io0_io]
set_property PACKAGE_PIN K18 [get_ports spi_cfg_io1_io]
set_property PACKAGE_PIN L14 [get_ports spi_cfg_io2_io]
set_property PACKAGE_PIN M14 [get_ports spi_cfg_io3_io]
set_property PACKAGE_PIN L16 [get_ports spi_cfg_sck_io]


#bitgen settings
set_property CONFIG_MODE SPIx4 [current_design]
#set_property BITSTREAM.CONFIG.SPI_32BIT_ADDR YES [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
set_property BITSTREAM.CONFIG.SPI_FALL_EDGE YES [current_design]
set_property BITSTREAM.CONFIG.UNUSEDPIN PULLNONE [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 66 [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]
set_property CFGBVS VCCO [current_design]
set_property BITSTREAM.CONFIG.M0PIN PULLUP [current_design]
set_property BITSTREAM.CONFIG.M1PIN PULLDOWN [current_design]
set_property BITSTREAM.CONFIG.M2PIN PULLDOWN [current_design]


set_property PACKAGE_PIN E3 [get_ports sys_clock]




set_property IOSTANDARD LVCMOS33 [get_ports usb_uart_rxd]
set_property IOSTANDARD LVCMOS33 [get_ports usb_uart_txd]
set_property IOSTANDARD LVCMOS33 [get_ports TMP_RX]
set_property IOSTANDARD LVCMOS33 [get_ports TMP_TX]
set_property IOSTANDARD LVCMOS33 [get_ports BackingPump_RX]
set_property IOSTANDARD LVCMOS33 [get_ports BackingPump_TX]
set_property IOSTANDARD LVCMOS33 [get_ports HighG_RX]
set_property IOSTANDARD LVCMOS33 [get_ports HighG_TX]
set_property IOSTANDARD LVCMOS33 [get_ports LowG_RX]
set_property IOSTANDARD LVCMOS33 [get_ports LowG_TX]

set_property IOSTANDARD LVCMOS33 [get_ports Laser_RX]
set_property IOSTANDARD LVCMOS33 [get_ports Laser_TX]

set_property IOSTANDARD LVCMOS33 [get_ports debug_RX]
set_property IOSTANDARD LVCMOS33 [get_ports debug_TX]
set_property PACKAGE_PIN T11 [get_ports debug_RX]
set_property PACKAGE_PIN P14 [get_ports debug_TX]
set_property PACKAGE_PIN G13 [get_ports Laser_RX]
set_property PACKAGE_PIN B11 [get_ports Laser_TX]

set_property PACKAGE_PIN M17 [get_ports HighG_RX]
set_property PACKAGE_PIN P17 [get_ports HighG_TX]
set_property PACKAGE_PIN L18 [get_ports TMP_RX]
set_property PACKAGE_PIN M18 [get_ports TMP_TX]
set_property PACKAGE_PIN R17 [get_ports LowG_RX]
set_property PACKAGE_PIN U18 [get_ports LowG_TX]

set_property PACKAGE_PIN V17 [get_ports BackingPump_RX]
set_property PACKAGE_PIN M16 [get_ports BackingPump_TX]

# Problem ports: LowG_RX, LowG_TX, HighG_RX, HighG_TX, TMP_RX, TMP_TX.
set_property PACKAGE_PIN P15 [get_ports {gpio2_1_tri_io[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_1_tri_io[15]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio2_1_tri_io[14]}]
set_property PACKAGE_PIN U16 [get_ports {gpio2_1_tri_io[14]}]


set_property PACKAGE_PIN E15 [get_ports {gpio2_1_tri_io[15]}]
