//Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2016.4 (win64) Build 1756540 Mon Jan 23 19:11:23 MST 2017
//Date        : Wed Dec 13 17:37:52 2017
//Host        : DMITRY-IGNATYEV running 64-bit major release  (build 9200)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (BackingPump_RX,
    BackingPump_TX,
    HighG_RX,
    HighG_TX,
    Laser_RX,
    Laser_TX,
    LowG_RX,
    LowG_TX,
    TMP_RX,
    TMP_TX,
    ddr3_sdram_addr,
    ddr3_sdram_ba,
    ddr3_sdram_cas_n,
    ddr3_sdram_ck_n,
    ddr3_sdram_ck_p,
    ddr3_sdram_cke,
    ddr3_sdram_cs_n,
    ddr3_sdram_dm,
    ddr3_sdram_dq,
    ddr3_sdram_dqs_n,
    ddr3_sdram_dqs_p,
    ddr3_sdram_odt,
    ddr3_sdram_ras_n,
    ddr3_sdram_reset_n,
    ddr3_sdram_we_n,
    debug_RX,
    debug_TX,
    dip_switches_4bits_tri_i,
    eth_mdio_mdc_mdc,
    eth_mdio_mdc_mdio_io,
    eth_mii_col,
    eth_mii_crs,
    eth_mii_rst_n,
    eth_mii_rx_clk,
    eth_mii_rx_dv,
    eth_mii_rx_er,
    eth_mii_rxd,
    eth_mii_tx_clk,
    eth_mii_tx_en,
    eth_mii_txd,
    eth_phy_clk,
    gpio2_1_tri_io,
    gpio2_2_tri_io,
    led_4bits_tri_io,
    push_buttons_4bits_tri_i,
    reset,
    rgb_led_tri_io,
    robot_rx,
    robot_tx,
    spi_0_io0_io,
    spi_0_io1_io,
    spi_0_io2_io,
    spi_0_io3_io,
    spi_0_sck_io,
    spi_0_ss_io,
    spi_cfg_io0_io,
    spi_cfg_io1_io,
    spi_cfg_io2_io,
    spi_cfg_io3_io,
    spi_cfg_sck_io,
    spi_cfg_ss_io,
    sys_clock,
    usb_uart_rxd,
    usb_uart_txd);
  input BackingPump_RX;
  output BackingPump_TX;
  input HighG_RX;
  output HighG_TX;
  input Laser_RX;
  output Laser_TX;
  input LowG_RX;
  output LowG_TX;
  input TMP_RX;
  output TMP_TX;
  output [13:0]ddr3_sdram_addr;
  output [2:0]ddr3_sdram_ba;
  output ddr3_sdram_cas_n;
  output [0:0]ddr3_sdram_ck_n;
  output [0:0]ddr3_sdram_ck_p;
  output [0:0]ddr3_sdram_cke;
  output [0:0]ddr3_sdram_cs_n;
  output [1:0]ddr3_sdram_dm;
  inout [15:0]ddr3_sdram_dq;
  inout [1:0]ddr3_sdram_dqs_n;
  inout [1:0]ddr3_sdram_dqs_p;
  output [0:0]ddr3_sdram_odt;
  output ddr3_sdram_ras_n;
  output ddr3_sdram_reset_n;
  output ddr3_sdram_we_n;
  input debug_RX;
  output debug_TX;
  input [3:0]dip_switches_4bits_tri_i;
  output eth_mdio_mdc_mdc;
  inout eth_mdio_mdc_mdio_io;
  input eth_mii_col;
  input eth_mii_crs;
  output eth_mii_rst_n;
  input eth_mii_rx_clk;
  input eth_mii_rx_dv;
  input eth_mii_rx_er;
  input [3:0]eth_mii_rxd;
  input eth_mii_tx_clk;
  output eth_mii_tx_en;
  output [3:0]eth_mii_txd;
  output eth_phy_clk;
  inout [12:0]gpio2_1_tri_io;
  inout [2:0]gpio2_2_tri_io;
  inout [3:0]led_4bits_tri_io;
  input [3:0]push_buttons_4bits_tri_i;
  input reset;
  inout [11:0]rgb_led_tri_io;
  input robot_rx;
  output robot_tx;
  inout spi_0_io0_io;
  inout spi_0_io1_io;
  inout spi_0_io2_io;
  inout spi_0_io3_io;
  inout spi_0_sck_io;
  inout [0:0]spi_0_ss_io;
  inout spi_cfg_io0_io;
  inout spi_cfg_io1_io;
  inout spi_cfg_io2_io;
  inout spi_cfg_io3_io;
  inout spi_cfg_sck_io;
  inout [0:0]spi_cfg_ss_io;
  input sys_clock;
  input usb_uart_rxd;
  output usb_uart_txd;

  wire BackingPump_RX;
  wire BackingPump_TX;
  wire HighG_RX;
  wire HighG_TX;
  wire Laser_RX;
  wire Laser_TX;
  wire LowG_RX;
  wire LowG_TX;
  wire TMP_RX;
  wire TMP_TX;
  wire [13:0]ddr3_sdram_addr;
  wire [2:0]ddr3_sdram_ba;
  wire ddr3_sdram_cas_n;
  wire [0:0]ddr3_sdram_ck_n;
  wire [0:0]ddr3_sdram_ck_p;
  wire [0:0]ddr3_sdram_cke;
  wire [0:0]ddr3_sdram_cs_n;
  wire [1:0]ddr3_sdram_dm;
  wire [15:0]ddr3_sdram_dq;
  wire [1:0]ddr3_sdram_dqs_n;
  wire [1:0]ddr3_sdram_dqs_p;
  wire [0:0]ddr3_sdram_odt;
  wire ddr3_sdram_ras_n;
  wire ddr3_sdram_reset_n;
  wire ddr3_sdram_we_n;
  wire debug_RX;
  wire debug_TX;
  wire [3:0]dip_switches_4bits_tri_i;
  wire eth_mdio_mdc_mdc;
  wire eth_mdio_mdc_mdio_i;
  wire eth_mdio_mdc_mdio_io;
  wire eth_mdio_mdc_mdio_o;
  wire eth_mdio_mdc_mdio_t;
  wire eth_mii_col;
  wire eth_mii_crs;
  wire eth_mii_rst_n;
  wire eth_mii_rx_clk;
  wire eth_mii_rx_dv;
  wire eth_mii_rx_er;
  wire [3:0]eth_mii_rxd;
  wire eth_mii_tx_clk;
  wire eth_mii_tx_en;
  wire [3:0]eth_mii_txd;
  wire eth_phy_clk;
  wire [0:0]gpio2_1_tri_i_0;
  wire [1:1]gpio2_1_tri_i_1;
  wire [10:10]gpio2_1_tri_i_10;
  wire [11:11]gpio2_1_tri_i_11;
  wire [12:12]gpio2_1_tri_i_12;
  wire [2:2]gpio2_1_tri_i_2;
  wire [3:3]gpio2_1_tri_i_3;
  wire [4:4]gpio2_1_tri_i_4;
  wire [5:5]gpio2_1_tri_i_5;
  wire [6:6]gpio2_1_tri_i_6;
  wire [7:7]gpio2_1_tri_i_7;
  wire [8:8]gpio2_1_tri_i_8;
  wire [9:9]gpio2_1_tri_i_9;
  wire [0:0]gpio2_1_tri_io_0;
  wire [1:1]gpio2_1_tri_io_1;
  wire [10:10]gpio2_1_tri_io_10;
  wire [11:11]gpio2_1_tri_io_11;
  wire [12:12]gpio2_1_tri_io_12;
  wire [2:2]gpio2_1_tri_io_2;
  wire [3:3]gpio2_1_tri_io_3;
  wire [4:4]gpio2_1_tri_io_4;
  wire [5:5]gpio2_1_tri_io_5;
  wire [6:6]gpio2_1_tri_io_6;
  wire [7:7]gpio2_1_tri_io_7;
  wire [8:8]gpio2_1_tri_io_8;
  wire [9:9]gpio2_1_tri_io_9;
  wire [0:0]gpio2_1_tri_o_0;
  wire [1:1]gpio2_1_tri_o_1;
  wire [10:10]gpio2_1_tri_o_10;
  wire [11:11]gpio2_1_tri_o_11;
  wire [12:12]gpio2_1_tri_o_12;
  wire [2:2]gpio2_1_tri_o_2;
  wire [3:3]gpio2_1_tri_o_3;
  wire [4:4]gpio2_1_tri_o_4;
  wire [5:5]gpio2_1_tri_o_5;
  wire [6:6]gpio2_1_tri_o_6;
  wire [7:7]gpio2_1_tri_o_7;
  wire [8:8]gpio2_1_tri_o_8;
  wire [9:9]gpio2_1_tri_o_9;
  wire [0:0]gpio2_1_tri_t_0;
  wire [1:1]gpio2_1_tri_t_1;
  wire [10:10]gpio2_1_tri_t_10;
  wire [11:11]gpio2_1_tri_t_11;
  wire [12:12]gpio2_1_tri_t_12;
  wire [2:2]gpio2_1_tri_t_2;
  wire [3:3]gpio2_1_tri_t_3;
  wire [4:4]gpio2_1_tri_t_4;
  wire [5:5]gpio2_1_tri_t_5;
  wire [6:6]gpio2_1_tri_t_6;
  wire [7:7]gpio2_1_tri_t_7;
  wire [8:8]gpio2_1_tri_t_8;
  wire [9:9]gpio2_1_tri_t_9;
  wire [0:0]gpio2_2_tri_i_0;
  wire [1:1]gpio2_2_tri_i_1;
  wire [2:2]gpio2_2_tri_i_2;
  wire [0:0]gpio2_2_tri_io_0;
  wire [1:1]gpio2_2_tri_io_1;
  wire [2:2]gpio2_2_tri_io_2;
  wire [0:0]gpio2_2_tri_o_0;
  wire [1:1]gpio2_2_tri_o_1;
  wire [2:2]gpio2_2_tri_o_2;
  wire [0:0]gpio2_2_tri_t_0;
  wire [1:1]gpio2_2_tri_t_1;
  wire [2:2]gpio2_2_tri_t_2;
  wire [0:0]led_4bits_tri_i_0;
  wire [1:1]led_4bits_tri_i_1;
  wire [2:2]led_4bits_tri_i_2;
  wire [3:3]led_4bits_tri_i_3;
  wire [0:0]led_4bits_tri_io_0;
  wire [1:1]led_4bits_tri_io_1;
  wire [2:2]led_4bits_tri_io_2;
  wire [3:3]led_4bits_tri_io_3;
  wire [0:0]led_4bits_tri_o_0;
  wire [1:1]led_4bits_tri_o_1;
  wire [2:2]led_4bits_tri_o_2;
  wire [3:3]led_4bits_tri_o_3;
  wire [0:0]led_4bits_tri_t_0;
  wire [1:1]led_4bits_tri_t_1;
  wire [2:2]led_4bits_tri_t_2;
  wire [3:3]led_4bits_tri_t_3;
  wire [3:0]push_buttons_4bits_tri_i;
  wire reset;
  wire [0:0]rgb_led_tri_i_0;
  wire [1:1]rgb_led_tri_i_1;
  wire [10:10]rgb_led_tri_i_10;
  wire [11:11]rgb_led_tri_i_11;
  wire [2:2]rgb_led_tri_i_2;
  wire [3:3]rgb_led_tri_i_3;
  wire [4:4]rgb_led_tri_i_4;
  wire [5:5]rgb_led_tri_i_5;
  wire [6:6]rgb_led_tri_i_6;
  wire [7:7]rgb_led_tri_i_7;
  wire [8:8]rgb_led_tri_i_8;
  wire [9:9]rgb_led_tri_i_9;
  wire [0:0]rgb_led_tri_io_0;
  wire [1:1]rgb_led_tri_io_1;
  wire [10:10]rgb_led_tri_io_10;
  wire [11:11]rgb_led_tri_io_11;
  wire [2:2]rgb_led_tri_io_2;
  wire [3:3]rgb_led_tri_io_3;
  wire [4:4]rgb_led_tri_io_4;
  wire [5:5]rgb_led_tri_io_5;
  wire [6:6]rgb_led_tri_io_6;
  wire [7:7]rgb_led_tri_io_7;
  wire [8:8]rgb_led_tri_io_8;
  wire [9:9]rgb_led_tri_io_9;
  wire [0:0]rgb_led_tri_o_0;
  wire [1:1]rgb_led_tri_o_1;
  wire [10:10]rgb_led_tri_o_10;
  wire [11:11]rgb_led_tri_o_11;
  wire [2:2]rgb_led_tri_o_2;
  wire [3:3]rgb_led_tri_o_3;
  wire [4:4]rgb_led_tri_o_4;
  wire [5:5]rgb_led_tri_o_5;
  wire [6:6]rgb_led_tri_o_6;
  wire [7:7]rgb_led_tri_o_7;
  wire [8:8]rgb_led_tri_o_8;
  wire [9:9]rgb_led_tri_o_9;
  wire [0:0]rgb_led_tri_t_0;
  wire [1:1]rgb_led_tri_t_1;
  wire [10:10]rgb_led_tri_t_10;
  wire [11:11]rgb_led_tri_t_11;
  wire [2:2]rgb_led_tri_t_2;
  wire [3:3]rgb_led_tri_t_3;
  wire [4:4]rgb_led_tri_t_4;
  wire [5:5]rgb_led_tri_t_5;
  wire [6:6]rgb_led_tri_t_6;
  wire [7:7]rgb_led_tri_t_7;
  wire [8:8]rgb_led_tri_t_8;
  wire [9:9]rgb_led_tri_t_9;
  wire robot_rx;
  wire robot_tx;
  wire spi_0_io0_i;
  wire spi_0_io0_io;
  wire spi_0_io0_o;
  wire spi_0_io0_t;
  wire spi_0_io1_i;
  wire spi_0_io1_io;
  wire spi_0_io1_o;
  wire spi_0_io1_t;
  wire spi_0_io2_i;
  wire spi_0_io2_io;
  wire spi_0_io2_o;
  wire spi_0_io2_t;
  wire spi_0_io3_i;
  wire spi_0_io3_io;
  wire spi_0_io3_o;
  wire spi_0_io3_t;
  wire spi_0_sck_i;
  wire spi_0_sck_io;
  wire spi_0_sck_o;
  wire spi_0_sck_t;
  wire [0:0]spi_0_ss_i_0;
  wire [0:0]spi_0_ss_io_0;
  wire [0:0]spi_0_ss_o_0;
  wire spi_0_ss_t;
  wire spi_cfg_io0_i;
  wire spi_cfg_io0_io;
  wire spi_cfg_io0_o;
  wire spi_cfg_io0_t;
  wire spi_cfg_io1_i;
  wire spi_cfg_io1_io;
  wire spi_cfg_io1_o;
  wire spi_cfg_io1_t;
  wire spi_cfg_io2_i;
  wire spi_cfg_io2_io;
  wire spi_cfg_io2_o;
  wire spi_cfg_io2_t;
  wire spi_cfg_io3_i;
  wire spi_cfg_io3_io;
  wire spi_cfg_io3_o;
  wire spi_cfg_io3_t;
  wire spi_cfg_sck_i;
  wire spi_cfg_sck_io;
  wire spi_cfg_sck_o;
  wire spi_cfg_sck_t;
  wire [0:0]spi_cfg_ss_i_0;
  wire [0:0]spi_cfg_ss_io_0;
  wire [0:0]spi_cfg_ss_o_0;
  wire spi_cfg_ss_t;
  wire sys_clock;
  wire usb_uart_rxd;
  wire usb_uart_txd;

  design_1 design_1_i
       (.BackingPump_RX(BackingPump_RX),
        .BackingPump_TX(BackingPump_TX),
        .GPIO2_1_tri_i({gpio2_1_tri_i_12,gpio2_1_tri_i_11,gpio2_1_tri_i_10,gpio2_1_tri_i_9,gpio2_1_tri_i_8,gpio2_1_tri_i_7,gpio2_1_tri_i_6,gpio2_1_tri_i_5,gpio2_1_tri_i_4,gpio2_1_tri_i_3,gpio2_1_tri_i_2,gpio2_1_tri_i_1,gpio2_1_tri_i_0}),
        .GPIO2_1_tri_o({gpio2_1_tri_o_12,gpio2_1_tri_o_11,gpio2_1_tri_o_10,gpio2_1_tri_o_9,gpio2_1_tri_o_8,gpio2_1_tri_o_7,gpio2_1_tri_o_6,gpio2_1_tri_o_5,gpio2_1_tri_o_4,gpio2_1_tri_o_3,gpio2_1_tri_o_2,gpio2_1_tri_o_1,gpio2_1_tri_o_0}),
        .GPIO2_1_tri_t({gpio2_1_tri_t_12,gpio2_1_tri_t_11,gpio2_1_tri_t_10,gpio2_1_tri_t_9,gpio2_1_tri_t_8,gpio2_1_tri_t_7,gpio2_1_tri_t_6,gpio2_1_tri_t_5,gpio2_1_tri_t_4,gpio2_1_tri_t_3,gpio2_1_tri_t_2,gpio2_1_tri_t_1,gpio2_1_tri_t_0}),
        .GPIO2_2_tri_i({gpio2_2_tri_i_2,gpio2_2_tri_i_1,gpio2_2_tri_i_0}),
        .GPIO2_2_tri_o({gpio2_2_tri_o_2,gpio2_2_tri_o_1,gpio2_2_tri_o_0}),
        .GPIO2_2_tri_t({gpio2_2_tri_t_2,gpio2_2_tri_t_1,gpio2_2_tri_t_0}),
        .HighG_RX(HighG_RX),
        .HighG_TX(HighG_TX),
        .Laser_RX(Laser_RX),
        .Laser_TX(Laser_TX),
        .LowG_RX(LowG_RX),
        .LowG_TX(LowG_TX),
        .SPI_0_io0_i(spi_0_io0_i),
        .SPI_0_io0_o(spi_0_io0_o),
        .SPI_0_io0_t(spi_0_io0_t),
        .SPI_0_io1_i(spi_0_io1_i),
        .SPI_0_io1_o(spi_0_io1_o),
        .SPI_0_io1_t(spi_0_io1_t),
        .SPI_0_io2_i(spi_0_io2_i),
        .SPI_0_io2_o(spi_0_io2_o),
        .SPI_0_io2_t(spi_0_io2_t),
        .SPI_0_io3_i(spi_0_io3_i),
        .SPI_0_io3_o(spi_0_io3_o),
        .SPI_0_io3_t(spi_0_io3_t),
        .SPI_0_sck_i(spi_0_sck_i),
        .SPI_0_sck_o(spi_0_sck_o),
        .SPI_0_sck_t(spi_0_sck_t),
        .SPI_0_ss_i(spi_0_ss_i_0),
        .SPI_0_ss_o(spi_0_ss_o_0),
        .SPI_0_ss_t(spi_0_ss_t),
        .SPI_CFG_io0_i(spi_cfg_io0_i),
        .SPI_CFG_io0_o(spi_cfg_io0_o),
        .SPI_CFG_io0_t(spi_cfg_io0_t),
        .SPI_CFG_io1_i(spi_cfg_io1_i),
        .SPI_CFG_io1_o(spi_cfg_io1_o),
        .SPI_CFG_io1_t(spi_cfg_io1_t),
        .SPI_CFG_io2_i(spi_cfg_io2_i),
        .SPI_CFG_io2_o(spi_cfg_io2_o),
        .SPI_CFG_io2_t(spi_cfg_io2_t),
        .SPI_CFG_io3_i(spi_cfg_io3_i),
        .SPI_CFG_io3_o(spi_cfg_io3_o),
        .SPI_CFG_io3_t(spi_cfg_io3_t),
        .SPI_CFG_sck_i(spi_cfg_sck_i),
        .SPI_CFG_sck_o(spi_cfg_sck_o),
        .SPI_CFG_sck_t(spi_cfg_sck_t),
        .SPI_CFG_ss_i(spi_cfg_ss_i_0),
        .SPI_CFG_ss_o(spi_cfg_ss_o_0),
        .SPI_CFG_ss_t(spi_cfg_ss_t),
        .TMP_RX(TMP_RX),
        .TMP_TX(TMP_TX),
        .ddr3_sdram_addr(ddr3_sdram_addr),
        .ddr3_sdram_ba(ddr3_sdram_ba),
        .ddr3_sdram_cas_n(ddr3_sdram_cas_n),
        .ddr3_sdram_ck_n(ddr3_sdram_ck_n),
        .ddr3_sdram_ck_p(ddr3_sdram_ck_p),
        .ddr3_sdram_cke(ddr3_sdram_cke),
        .ddr3_sdram_cs_n(ddr3_sdram_cs_n),
        .ddr3_sdram_dm(ddr3_sdram_dm),
        .ddr3_sdram_dq(ddr3_sdram_dq),
        .ddr3_sdram_dqs_n(ddr3_sdram_dqs_n),
        .ddr3_sdram_dqs_p(ddr3_sdram_dqs_p),
        .ddr3_sdram_odt(ddr3_sdram_odt),
        .ddr3_sdram_ras_n(ddr3_sdram_ras_n),
        .ddr3_sdram_reset_n(ddr3_sdram_reset_n),
        .ddr3_sdram_we_n(ddr3_sdram_we_n),
        .debug_RX(debug_RX),
        .debug_TX(debug_TX),
        .dip_switches_4bits_tri_i(dip_switches_4bits_tri_i),
        .eth_mdio_mdc_mdc(eth_mdio_mdc_mdc),
        .eth_mdio_mdc_mdio_i(eth_mdio_mdc_mdio_i),
        .eth_mdio_mdc_mdio_o(eth_mdio_mdc_mdio_o),
        .eth_mdio_mdc_mdio_t(eth_mdio_mdc_mdio_t),
        .eth_mii_col(eth_mii_col),
        .eth_mii_crs(eth_mii_crs),
        .eth_mii_rst_n(eth_mii_rst_n),
        .eth_mii_rx_clk(eth_mii_rx_clk),
        .eth_mii_rx_dv(eth_mii_rx_dv),
        .eth_mii_rx_er(eth_mii_rx_er),
        .eth_mii_rxd(eth_mii_rxd),
        .eth_mii_tx_clk(eth_mii_tx_clk),
        .eth_mii_tx_en(eth_mii_tx_en),
        .eth_mii_txd(eth_mii_txd),
        .eth_phy_clk(eth_phy_clk),
        .led_4bits_tri_i({led_4bits_tri_i_3,led_4bits_tri_i_2,led_4bits_tri_i_1,led_4bits_tri_i_0}),
        .led_4bits_tri_o({led_4bits_tri_o_3,led_4bits_tri_o_2,led_4bits_tri_o_1,led_4bits_tri_o_0}),
        .led_4bits_tri_t({led_4bits_tri_t_3,led_4bits_tri_t_2,led_4bits_tri_t_1,led_4bits_tri_t_0}),
        .push_buttons_4bits_tri_i(push_buttons_4bits_tri_i),
        .reset(reset),
        .rgb_led_tri_i({rgb_led_tri_i_11,rgb_led_tri_i_10,rgb_led_tri_i_9,rgb_led_tri_i_8,rgb_led_tri_i_7,rgb_led_tri_i_6,rgb_led_tri_i_5,rgb_led_tri_i_4,rgb_led_tri_i_3,rgb_led_tri_i_2,rgb_led_tri_i_1,rgb_led_tri_i_0}),
        .rgb_led_tri_o({rgb_led_tri_o_11,rgb_led_tri_o_10,rgb_led_tri_o_9,rgb_led_tri_o_8,rgb_led_tri_o_7,rgb_led_tri_o_6,rgb_led_tri_o_5,rgb_led_tri_o_4,rgb_led_tri_o_3,rgb_led_tri_o_2,rgb_led_tri_o_1,rgb_led_tri_o_0}),
        .rgb_led_tri_t({rgb_led_tri_t_11,rgb_led_tri_t_10,rgb_led_tri_t_9,rgb_led_tri_t_8,rgb_led_tri_t_7,rgb_led_tri_t_6,rgb_led_tri_t_5,rgb_led_tri_t_4,rgb_led_tri_t_3,rgb_led_tri_t_2,rgb_led_tri_t_1,rgb_led_tri_t_0}),
        .robot_rx(robot_rx),
        .robot_tx(robot_tx),
        .sys_clock(sys_clock),
        .usb_uart_rxd(usb_uart_rxd),
        .usb_uart_txd(usb_uart_txd));
  IOBUF eth_mdio_mdc_mdio_iobuf
       (.I(eth_mdio_mdc_mdio_o),
        .IO(eth_mdio_mdc_mdio_io),
        .O(eth_mdio_mdc_mdio_i),
        .T(eth_mdio_mdc_mdio_t));
  IOBUF gpio2_1_tri_iobuf_0
       (.I(gpio2_1_tri_o_0),
        .IO(gpio2_1_tri_io[0]),
        .O(gpio2_1_tri_i_0),
        .T(gpio2_1_tri_t_0));
  IOBUF gpio2_1_tri_iobuf_1
       (.I(gpio2_1_tri_o_1),
        .IO(gpio2_1_tri_io[1]),
        .O(gpio2_1_tri_i_1),
        .T(gpio2_1_tri_t_1));
  IOBUF gpio2_1_tri_iobuf_10
       (.I(gpio2_1_tri_o_10),
        .IO(gpio2_1_tri_io[10]),
        .O(gpio2_1_tri_i_10),
        .T(gpio2_1_tri_t_10));
  IOBUF gpio2_1_tri_iobuf_11
       (.I(gpio2_1_tri_o_11),
        .IO(gpio2_1_tri_io[11]),
        .O(gpio2_1_tri_i_11),
        .T(gpio2_1_tri_t_11));
  IOBUF gpio2_1_tri_iobuf_12
       (.I(gpio2_1_tri_o_12),
        .IO(gpio2_1_tri_io[12]),
        .O(gpio2_1_tri_i_12),
        .T(gpio2_1_tri_t_12));
  IOBUF gpio2_1_tri_iobuf_2
       (.I(gpio2_1_tri_o_2),
        .IO(gpio2_1_tri_io[2]),
        .O(gpio2_1_tri_i_2),
        .T(gpio2_1_tri_t_2));
  IOBUF gpio2_1_tri_iobuf_3
       (.I(gpio2_1_tri_o_3),
        .IO(gpio2_1_tri_io[3]),
        .O(gpio2_1_tri_i_3),
        .T(gpio2_1_tri_t_3));
  IOBUF gpio2_1_tri_iobuf_4
       (.I(gpio2_1_tri_o_4),
        .IO(gpio2_1_tri_io[4]),
        .O(gpio2_1_tri_i_4),
        .T(gpio2_1_tri_t_4));
  IOBUF gpio2_1_tri_iobuf_5
       (.I(gpio2_1_tri_o_5),
        .IO(gpio2_1_tri_io[5]),
        .O(gpio2_1_tri_i_5),
        .T(gpio2_1_tri_t_5));
  IOBUF gpio2_1_tri_iobuf_6
       (.I(gpio2_1_tri_o_6),
        .IO(gpio2_1_tri_io[6]),
        .O(gpio2_1_tri_i_6),
        .T(gpio2_1_tri_t_6));
  IOBUF gpio2_1_tri_iobuf_7
       (.I(gpio2_1_tri_o_7),
        .IO(gpio2_1_tri_io[7]),
        .O(gpio2_1_tri_i_7),
        .T(gpio2_1_tri_t_7));
  IOBUF gpio2_1_tri_iobuf_8
       (.I(gpio2_1_tri_o_8),
        .IO(gpio2_1_tri_io[8]),
        .O(gpio2_1_tri_i_8),
        .T(gpio2_1_tri_t_8));
  IOBUF gpio2_1_tri_iobuf_9
       (.I(gpio2_1_tri_o_9),
        .IO(gpio2_1_tri_io[9]),
        .O(gpio2_1_tri_i_9),
        .T(gpio2_1_tri_t_9));
  IOBUF gpio2_2_tri_iobuf_0
       (.I(gpio2_2_tri_o_0),
        .IO(gpio2_2_tri_io[0]),
        .O(gpio2_2_tri_i_0),
        .T(gpio2_2_tri_t_0));
  IOBUF gpio2_2_tri_iobuf_1
       (.I(gpio2_2_tri_o_1),
        .IO(gpio2_2_tri_io[1]),
        .O(gpio2_2_tri_i_1),
        .T(gpio2_2_tri_t_1));
  IOBUF gpio2_2_tri_iobuf_2
       (.I(gpio2_2_tri_o_2),
        .IO(gpio2_2_tri_io[2]),
        .O(gpio2_2_tri_i_2),
        .T(gpio2_2_tri_t_2));
  IOBUF led_4bits_tri_iobuf_0
       (.I(led_4bits_tri_o_0),
        .IO(led_4bits_tri_io[0]),
        .O(led_4bits_tri_i_0),
        .T(led_4bits_tri_t_0));
  IOBUF led_4bits_tri_iobuf_1
       (.I(led_4bits_tri_o_1),
        .IO(led_4bits_tri_io[1]),
        .O(led_4bits_tri_i_1),
        .T(led_4bits_tri_t_1));
  IOBUF led_4bits_tri_iobuf_2
       (.I(led_4bits_tri_o_2),
        .IO(led_4bits_tri_io[2]),
        .O(led_4bits_tri_i_2),
        .T(led_4bits_tri_t_2));
  IOBUF led_4bits_tri_iobuf_3
       (.I(led_4bits_tri_o_3),
        .IO(led_4bits_tri_io[3]),
        .O(led_4bits_tri_i_3),
        .T(led_4bits_tri_t_3));
  IOBUF rgb_led_tri_iobuf_0
       (.I(rgb_led_tri_o_0),
        .IO(rgb_led_tri_io[0]),
        .O(rgb_led_tri_i_0),
        .T(rgb_led_tri_t_0));
  IOBUF rgb_led_tri_iobuf_1
       (.I(rgb_led_tri_o_1),
        .IO(rgb_led_tri_io[1]),
        .O(rgb_led_tri_i_1),
        .T(rgb_led_tri_t_1));
  IOBUF rgb_led_tri_iobuf_10
       (.I(rgb_led_tri_o_10),
        .IO(rgb_led_tri_io[10]),
        .O(rgb_led_tri_i_10),
        .T(rgb_led_tri_t_10));
  IOBUF rgb_led_tri_iobuf_11
       (.I(rgb_led_tri_o_11),
        .IO(rgb_led_tri_io[11]),
        .O(rgb_led_tri_i_11),
        .T(rgb_led_tri_t_11));
  IOBUF rgb_led_tri_iobuf_2
       (.I(rgb_led_tri_o_2),
        .IO(rgb_led_tri_io[2]),
        .O(rgb_led_tri_i_2),
        .T(rgb_led_tri_t_2));
  IOBUF rgb_led_tri_iobuf_3
       (.I(rgb_led_tri_o_3),
        .IO(rgb_led_tri_io[3]),
        .O(rgb_led_tri_i_3),
        .T(rgb_led_tri_t_3));
  IOBUF rgb_led_tri_iobuf_4
       (.I(rgb_led_tri_o_4),
        .IO(rgb_led_tri_io[4]),
        .O(rgb_led_tri_i_4),
        .T(rgb_led_tri_t_4));
  IOBUF rgb_led_tri_iobuf_5
       (.I(rgb_led_tri_o_5),
        .IO(rgb_led_tri_io[5]),
        .O(rgb_led_tri_i_5),
        .T(rgb_led_tri_t_5));
  IOBUF rgb_led_tri_iobuf_6
       (.I(rgb_led_tri_o_6),
        .IO(rgb_led_tri_io[6]),
        .O(rgb_led_tri_i_6),
        .T(rgb_led_tri_t_6));
  IOBUF rgb_led_tri_iobuf_7
       (.I(rgb_led_tri_o_7),
        .IO(rgb_led_tri_io[7]),
        .O(rgb_led_tri_i_7),
        .T(rgb_led_tri_t_7));
  IOBUF rgb_led_tri_iobuf_8
       (.I(rgb_led_tri_o_8),
        .IO(rgb_led_tri_io[8]),
        .O(rgb_led_tri_i_8),
        .T(rgb_led_tri_t_8));
  IOBUF rgb_led_tri_iobuf_9
       (.I(rgb_led_tri_o_9),
        .IO(rgb_led_tri_io[9]),
        .O(rgb_led_tri_i_9),
        .T(rgb_led_tri_t_9));
  IOBUF spi_0_io0_iobuf
       (.I(spi_0_io0_o),
        .IO(spi_0_io0_io),
        .O(spi_0_io0_i),
        .T(spi_0_io0_t));
  IOBUF spi_0_io1_iobuf
       (.I(spi_0_io1_o),
        .IO(spi_0_io1_io),
        .O(spi_0_io1_i),
        .T(spi_0_io1_t));
  IOBUF spi_0_io2_iobuf
       (.I(spi_0_io2_o),
        .IO(spi_0_io2_io),
        .O(spi_0_io2_i),
        .T(spi_0_io2_t));
  IOBUF spi_0_io3_iobuf
       (.I(spi_0_io3_o),
        .IO(spi_0_io3_io),
        .O(spi_0_io3_i),
        .T(spi_0_io3_t));
  IOBUF spi_0_sck_iobuf
       (.I(spi_0_sck_o),
        .IO(spi_0_sck_io),
        .O(spi_0_sck_i),
        .T(spi_0_sck_t));
  IOBUF spi_0_ss_iobuf_0
       (.I(spi_0_ss_o_0),
        .IO(spi_0_ss_io[0]),
        .O(spi_0_ss_i_0),
        .T(spi_0_ss_t));
  IOBUF spi_cfg_io0_iobuf
       (.I(spi_cfg_io0_o),
        .IO(spi_cfg_io0_io),
        .O(spi_cfg_io0_i),
        .T(spi_cfg_io0_t));
  IOBUF spi_cfg_io1_iobuf
       (.I(spi_cfg_io1_o),
        .IO(spi_cfg_io1_io),
        .O(spi_cfg_io1_i),
        .T(spi_cfg_io1_t));
  IOBUF spi_cfg_io2_iobuf
       (.I(spi_cfg_io2_o),
        .IO(spi_cfg_io2_io),
        .O(spi_cfg_io2_i),
        .T(spi_cfg_io2_t));
  IOBUF spi_cfg_io3_iobuf
       (.I(spi_cfg_io3_o),
        .IO(spi_cfg_io3_io),
        .O(spi_cfg_io3_i),
        .T(spi_cfg_io3_t));
  IOBUF spi_cfg_sck_iobuf
       (.I(spi_cfg_sck_o),
        .IO(spi_cfg_sck_io),
        .O(spi_cfg_sck_i),
        .T(spi_cfg_sck_t));
  IOBUF spi_cfg_ss_iobuf_0
       (.I(spi_cfg_ss_o_0),
        .IO(spi_cfg_ss_io[0]),
        .O(spi_cfg_ss_i_0),
        .T(spi_cfg_ss_t));
endmodule
