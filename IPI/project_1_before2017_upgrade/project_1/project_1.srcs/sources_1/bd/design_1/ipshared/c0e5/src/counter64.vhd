library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.all;

entity counter64 is
    Port (
        clk_in : in  STD_LOGIC;
        reset  : in  STD_LOGIC;
        clk_out: out STD_LOGIC;
        value: out std_logic_vector(63 downto 0)
    );
end counter64;

architecture Behavioral of counter64 is
    signal temporal: STD_LOGIC;
    signal counter : integer range 0 to 833333 := 0;
    signal big_counter: unsigned(63 downto 0):= (others => '0');
begin
    frequency_divider: process (reset, clk_in) begin
        if (reset = '1') then
            temporal <= '0';
            counter <= 0;
        elsif rising_edge(clk_in) then
            if (counter = 833333) then
                temporal <= NOT(temporal);
                big_counter <=  big_counter + 1;
                counter <= 0;
            else
                counter <= counter + 1;
            end if;
        end if;
    end process;
    
    clk_out <= temporal;
    value <= std_logic_vector(big_counter);
end Behavioral;